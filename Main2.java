import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Day sunday = new Day("Sunday", new String[]{"Gyme get", "Kent 8 cek"});
        Day monday = new Day("Monday", new String[]{"Oynamaq", "Yatmaq"});
        Day tuesday = new Day("Tuesday", new String[]{"Kursa get"});
        Day wednesday = new Day("Wednesday", new String[]{"Qacmaq", "Zirpaket patlatmaq"});
        Day thursday = new Day("Thursday", new String[]{"Icmek", "Yemek"});
        Day friday = new Day("Friday", new String[]{"Alisveris", "Brawl stars oynamaq"});
        Day saturday = new Day("Saturday", new String[]{"Futbol oynamaq"});
        Week week = new Week(new Day[]{sunday, monday, tuesday, wednesday, thursday, friday, saturday});
        while (true) {
            System.out.println("Please, input the day of the week");
            String inputDay = sc.nextLine();
            inputDay = inputDay.trim().toLowerCase();
            int con1 = 0;
            for (Day day : week.days) {
                if (inputDay.equals(day.name.toLowerCase())) {
                    System.out.println(day);
                    con1 += 1;
                }
            }
            if (inputDay.equals("exit")) {
                System.out.println("See you ");
                break;
            }
            if (con1 == 0) {
                System.out.println("Your input is mistaken");
            }
            System.out.println();
        }
    }
}






