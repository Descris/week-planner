
public class Week {
    public Day[] days;


    public Week(Day[] days) {
        this.days = days;
    }
    public Week() {
    }
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Day day : days) {
            stringBuilder.append(day.toString()).append("\n");
        }
        return stringBuilder.toString();
    }


    }

