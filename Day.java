import java.util.Arrays;

public class Day {
    String name;
    String[] tasks;

    public Day(String name, String[] tasks) {
        this.name = name;
        this.tasks = tasks;

    }
    public String toString() {
        return "Day: " + name + "\nTasks: " + Arrays.toString(tasks);
    }

}
